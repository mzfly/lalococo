# 经纬度转换工具

#### Description
功能预览如下:
-------------------------------
【经纬度坐标转换工具V0.9】
依赖：OS、xlrd、math、xlutils模块
-------------------------------
请选择以下转换方式:
【1】WGS84转GCJ02 (默认)
【2】GCJ02转WGS84
【3】BD09转GCJ02
【0】退出程序
>2
【GCJ02转WGS84】
请输入Excel文件：
>

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
