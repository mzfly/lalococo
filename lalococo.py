# -*- coding: utf-8 -*-
# 验证坐标：http://www.gpsspg.com/maps.htm
# 注意：本算法转换的坐标会有厘米级的损失
import os
import sys
import xlrd
import xlwt
import math
import time
from xlutils.copy import copy


# 以下为算法部分
x_pi = 3.14159265358979324 * 3000.0 / 180.0
pi = 3.1415926535897932384626  # π
a = 6378245.0  # 长半轴
ee = 0.00669342162296594323  # 扁率

def wgs2gcj(wgs_lng, wgs_lat):
	"""
	WGS84转GCJ02(火星坐标系)
	:param wgs_lng:WGS84坐标系的经度
	:param wgs_lat:WGS84坐标系的纬度
	:return:
	"""
	if out_of_china(wgs_lng, wgs_lat):  # 判断是否在国内
		return wgs_lng, wgs_lat
	dlat = transformlat(wgs_lng - 105.0, wgs_lat- 35.0)
	dlng = transformlng(wgs_lng - 105.0, wgs_lat- 35.0)
	radlat = wgs_lat/ 180.0 * pi
	magic = math.sin(radlat)
	magic = 1 - ee * magic * magic
	sqrtmagic = math.sqrt(magic)
	dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi)
	dlng = (dlng * 180.0) / (a / sqrtmagic * math.cos(radlat) * pi)
	mglat = wgs_lat+ dlat
	mglng = wgs_lng + dlng
	return [mglng, mglat]
	#return mglng,mglat


def gcj2wgs(lng, lat):
	"""
	GCJ02(火星坐标系)转GPS84
	:param lng:火星坐标系的经度
	:param lat:火星坐标系纬度
	:return:
	"""
	if out_of_china(lng, lat):
		return lng, lat
	dlat = transformlat(lng - 105.0, lat - 35.0)
	dlng = transformlng(lng - 105.0, lat - 35.0)
	radlat = lat / 180.0 * pi
	magic = math.sin(radlat)
	magic = 1 - ee * magic * magic
	sqrtmagic = math.sqrt(magic)
	dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi)
	dlng = (dlng * 180.0) / (a / sqrtmagic * math.cos(radlat) * pi)
	mglat = lat + dlat
	mglng = lng + dlng
	return [lng * 2 - mglng, lat * 2 - mglat]


def bd2gcj(bd_lon, bd_lat):
	"""
	百度坐标系(BD-09)转火星坐标系(GCJ-02)
	百度——>谷歌、高德
	:param bd_lat:百度坐标纬度
	:param bd_lon:百度坐标经度
	:return:转换后的坐标列表形式
	"""
	x = bd_lon - 0.0065
	y = bd_lat - 0.006
	z = math.sqrt(x * x + y * y) - 0.00002 * math.sin(y * x_pi)
	theta = math.atan2(y, x) - 0.000003 * math.cos(x * x_pi)
	gg_lng = z * math.cos(theta)
	gg_lat = z * math.sin(theta)
	return [gg_lng, gg_lat]


def transformlat(lng, lat):
	ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + \
		0.1 * lng * lat + 0.2 * math.sqrt(math.fabs(lng))
	ret += (20.0 * math.sin(6.0 * lng * pi) + 20.0 *
			math.sin(2.0 * lng * pi)) * 2.0 / 3.0
	ret += (20.0 * math.sin(lat * pi) + 40.0 *
			math.sin(lat / 3.0 * pi)) * 2.0 / 3.0
	ret += (160.0 * math.sin(lat / 12.0 * pi) + 320 *
			math.sin(lat * pi / 30.0)) * 2.0 / 3.0
	return ret


def transformlng(lng, lat):
	ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + \
		0.1 * lng * lat + 0.1 * math.sqrt(math.fabs(lng))
	ret += (20.0 * math.sin(6.0 * lng * pi) + 20.0 *
			math.sin(2.0 * lng * pi)) * 2.0 / 3.0
	ret += (20.0 * math.sin(lng * pi) + 40.0 *
			math.sin(lng / 3.0 * pi)) * 2.0 / 3.0
	ret += (150.0 * math.sin(lng / 12.0 * pi) + 300.0 *
			math.sin(lng / 30.0 * pi)) * 2.0 / 3.0
	return ret


def out_of_china(lng, lat):
	"""
	判断是否在国内，不在国内不做偏移
	:param lng:
	:param lat:
	:return:
	"""
	if lng < 72.004 or lng > 137.8347:
		return True
	if lat < 0.8293 or lat > 55.8271:
		return True
	return False


def ConvertInExcel(fileName,Callback,locx_index=0,locy_index=1):
	"""
	读取Excel里的坐标数据，转换坐标后保存到新建Excel里
	:param fileName：文件绝对路径
	:Callback：回调函数，可选 wgs2gcj 和 gcj2wgs
	:param locx_index:经度坐标在表的列索引，默认第1列
	:param locy_index:纬度坐标在表的列索引，默认第2列
	:return:返回转换坐标个数和保存文件路径
	"""
	try:
		workbook = xlrd.open_workbook(fileName)
	except:
		return (-1,-1)
	print("成功读取文件",end='\r')
	# 通过sheet索引获取所需sheet工资表对象
	sheet = workbook.sheet_by_index(0)
	locationsX = sheet.col_values(locx_index) # GCJ经度所在列
	locationsY = sheet.col_values(locy_index) # GCJ纬度所在列

	# 通过复制一个工作簿实现对已有内容的excel文件的写入。
	# 方式为：复制原工作簿，获取工作表，在新表下写入，保存时名称可以与源文件一致。
	wgsWorkbook = copy(workbook)
	# 获取第一个工作表
	wgsSheet = wgsWorkbook.get_sheet(0)
	# 在最右新列第一行写入“wgs_x”,.write参数：行，列，内容
	wgsSheet.write(0,sheet.ncols,Callback.__name__+"_X")
	# 在最右新列加1列第一行写入“wgs_y”
	wgsSheet.write(0,sheet.ncols+1,Callback.__name__+"_Y")

	print("转换坐标:          ")
	for i in range(1,sheet.nrows):
		try:
			# 调用 Callback 方法，返回转换后的坐标元组
			loc = Callback(locationsX[i],locationsY[i])
			# 在新的一列(wgs_x所在列第二行开始)写入转换后的坐标
			wgsSheet.write(i,sheet.ncols,loc[0])
			# 在新的一列+1(wgs_y所在列第二行开始)写入转换后的坐标
			wgsSheet.write(i,sheet.ncols+1,loc[1])
			print(i,end="\r")
		except:
			print('已跳过：第%s行转换失败（%s,%s）'%(str(i+1),locationsX[i],locationsY[i]))
			pass
	fname, ext = os.path.splitext(fileName)
	saveFileName = fname+"_"+Callback.__name__+ext


	# 把写好的内容另存为文件
	for i in range(1,65535):
		# 检查文件是否已经存在，存在则添加数字后缀
		if os.path.exists(saveFileName):
			saveFileName = fname+"_"+Callback.__name__+"_"+str(i)+ext;
		else:
			print("正在保存文件...          ",end='\r')
			wgsWorkbook.save(saveFileName)
			print("OK!共处理了%s行。"%sheet.nrows)
			print("转换后的数据已保存到："+str(saveFileName))
			return sheet.nrows,saveFileName
	return (-1,-1)


def InteractiveModel():
	"""
	InteractiveModel 函数传入
		excel表格的绝对路径
		回调函数：gcj2wgs、wgs2gcj
		经度列索引：
		纬度列索引：
	返回转换坐标个数和保存文件路径
	"""
	print("-------------------------------")
	print("【经纬度坐标转换工具V0.9】")
	print("依赖：OS、xlrd、math、xlutils模块")
	print("-------------------------------")
	select = {"0":"退出程序","":wgs2gcj,"1":wgs2gcj,"2":gcj2wgs,"3":bd2gcj}
	for t in range(1,999):
		print("请选择以下转换方式:\
			\n【1】WGS84转GCJ02 (默认)\
			\n【2】GCJ02转WGS84\
			\n【3】BD09转GCJ02\
			\n【0】退出程序")

		for i in range(1,10):
			inputSelect = input(">")
			if inputSelect=="0":
				print('【退出程序】')
				return
			elif inputSelect=="1" or inputSelect=="":
				print("【WGS84转GCJ02】")
				break 
			elif inputSelect=="2":
				print("【GCJ02转WGS84】")
				break
			elif inputSelect == "3":
				print("【BD09转GCJ02】")
				break
			else:
				print("输入无效，请重新输入")

		for i in range(1,10):
			sheet_nrowsrun,saveFileName = ConvertInExcel(input("请输入Excel文件：\n>"),select[inputSelect],locx_index=0,locy_index=1)
			if(sheet_nrowsrun==-1):
				# sheet_nrowsrun==-1 表示读取失败
				print("文件读取失败，请检查路径是否正确。")
			else:
				time.sleep(5)
				print("-------------------------------")
				break


if __name__ == "__main__":
	# 交互模式:CMD:python3 gcj2wgs_v5.py -i
	x=os.system('cls')
	# 判断命令行是否传入参数
	if(len(sys.argv)>1):
		# 判断传入的参数是否是以交互模式运行
		if(sys.argv[1]=='-i'):
			# 以命令行交互模式运行
			InteractiveModel()
		# 判断传入的参数是否是以自动模式运行
		elif(sys.argv[1]=='-c'):
			pass
	else:
		# 以以下代码运行
		# try:
		"""
		ConvertByExcel 函数传入
			excel表格的绝对路径
			回调函数：gcj2wgs、wgs2gcj、bd2gcj
			经度列索引：
			纬度列索引：
		返回转换坐标个数和保存文件路径
		"""
		try:
			sheet_nrowsrun,saveFileName = ConvertInExcel("E:/代码/python经纬度坐标转换/GCJ坐标数据.xls",wgs2gcj,locx_index=0,locy_index=1)
			if(sheet_nrowsrun==-1):
				# sheet_nrowsrun==-1 表示读取失败
				print("文件读取失败，请检查路径是否正确。")
				input("退出>")
			else:
				print("-------------------------------")
				input("退出>")
		except Exception as e:
			input("退出>")
